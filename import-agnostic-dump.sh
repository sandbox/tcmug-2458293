#!/usr/bin/env php
<?php
define('DRUPAL_ROOT', getcwd());
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';

try {
  drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);
}
catch (PDOException $e) {
  // A PDO exception might mean that we are running on an empty database. In
  // that case, just redirect the user to install.php.
  if (!db_table_exists('variable')) {
    include_once DRUPAL_ROOT . '/includes/install.inc';
    install_goto('install.php');
  }
  throw $e;
}

if (!isset($argv[1])) {
  echo "Enter a file name too!\n";
  exit(0);
}

include "sites/all/modules/contrib/agnosticdump/agnosticdump.drush.inc";
drush_agnosticdump_agnostic_import($argv[1]);

