<?php

/**
 * @file
 * Drush commands for using agnostic dump.
 */

/**
 * Implements hook_drush_command().
 */
function agnosticdump_drush_command() {

  $items['agnostic-dump'] = array(
    'description' => 'Dump the database in JSON format.',
    'aliases' => array('agd'),
  );

  $items['agnostic-import'] = array(
    'description' => 'Import from given file.',
    'aliases' => array('agi'),
    'arguments' => array(
      'filename' => 'Name of the file from which to import.',
    ),
  );

  return $items;
}

/**
 * Drush command for dumping the database in json format.
 */
function drush_agnosticdump_agnostic_dump() {

  $schema = drupal_get_schema(NULL, TRUE);
  // Get List Of all tables.
  ksort($schema);

  // How many records per line.
  $record_size = 100;

  $skip_tables = array('watchdog', 'autocache_cache_url_table');

  // Loop through schemas.
  foreach ($schema as $table_name => $value) {

    $rows = array();

    $export = array(
      $table_name => array(
        'schema' => drupal_get_schema($table_name, TRUE),
        'rows' => &$rows,
      ),
    );

    $fields = array_keys($export[$table_name]['schema']['fields']);
    $fields = implode(', ', $fields);

    $records = 0;

    if (strpos($table_name, 'cache') !== 0 && !in_array($table_name, $skip_tables)) {

      // Table is good for exporting.
      $at = 0;

      while (TRUE) {

        $db_rows = db_query("SELECT $fields FROM {$table_name} LIMIT $at, $record_size")->fetchAll();

        // No rows, exit loop.
        if (count($db_rows) == 0) {
          break;
        }

        // Keep count of where we are at.
        $at += count($db_rows);
        $records += count($db_rows);

        // Transform the items to an array.
        array_walk($db_rows, function (&$item, $key) {
          $item = array_values((array) $item);
        });

        // Echo records.
        $rows = $db_rows;
        echo drupal_json_encode($export) . PHP_EOL;

        // Remove schema for the next rows.
        unset($export[$table_name]['schema']);

      }

    }

    // Echo empty record.
    if ($records == 0) {
      echo drupal_json_encode($export) . PHP_EOL;
    }

  }

}


/**
 * Drush command for reading in a json database dump.
 */
function drush_agnosticdump_agnostic_import($filename = FALSE) {

  if (!$filename) {
    return;
  }

  // How many records per insert.
  $record_size = 100;

  $truncated = array();
  $handle = fopen($filename, "r");

  $processed_tables = array();

  if ($handle) {

    while (($line = fgets($handle)) !== FALSE) {

      $data = drupal_json_decode($line);

      foreach ($data as $table_name => $table_data) {

        // If the row contains a schema, get the field structure out of it.
        if (isset($table_data['schema'])) {

          $fields = array_keys($table_data['schema']['fields']);

          // Drop the table if it exists.
          if (db_table_exists($table_name)) {
            db_drop_table($table_name);
          }

          db_create_table($table_name, $table_data['schema']);
        }

        // Print out we're processing the table.
        if (!in_array($table_name, $processed_tables)) {
          echo " - $table_name" . PHP_EOL;
          $processed_tables[] = $table_name;
        }

        $query = db_insert($table_name)->fields($fields);
        $count = 0;

        foreach ($table_data['rows'] as $row) {
          $query->values(array_combine($fields, $row));
          $count++;
          if ($count == $record_size) {
            // Max insert size reached, run the query.
            $query->execute();
            $query = db_insert($table_name)->fields($fields);
            $count = 0;
          }
        }

        // Insert remaining rows.
        if ($count > 0) {
          $query->execute();
        }
      }
    }

    fclose($handle);

  }
  else {

    // Error opening the file.
    print "Could not open $filename" . PHP_EOL;

  }

}
