
This module exists to provide database agnostic database dumps
for migrating content from a database to another.

If you have a existing and drupal installation:

  Use drush:

    $ drush agnostic-dump > out.dump
    $ drush agnostic-import out.dump

  Or with drush shortcuts:

    $ drush agd > out.dump
    $ drush agi out.dump

If you have a broken installation where drush does not work:

  1. Copy import-agnostic-dump.sh to your drupal root folder.

  2. Run:

    $ ./import-agnostic-dump.sh mydump.json
